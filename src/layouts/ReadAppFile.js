import MobileDeviceFrame from "./MobileDeviceFrame";
import ScreenDisplay from "./ScreenDisplay";
import ElectricMeter2 from "./ElectricMeter2";
import { useEffect, useState } from "react";
import { setComp } from "../redux/Components/compSlice";
import { useDispatch, useSelector } from "react-redux";

function ReadAppFile() {
  const [config, setConfig] = useState("null");
  const dispatch = useDispatch();
  const groupFunction = useSelector((state) => state.groupFunction);
  const nameScreen = useSelector((state) => state.nameScreen);

  useEffect(() => {
    let response = [];
    console.log(groupFunction);
    console.log(nameScreen);
    if (
      config !== null &&
      groupFunction !== "welcome_screen" &&
      groupFunction !== "container"
    ) {
      response = [...config[groupFunction].screens[0].view.area];
      dispatch(setComp(response));
    }
    if (groupFunction === "container" && nameScreen === "") {
      response = [...config[groupFunction].screens[0].screens[0].view.area];
      dispatch(setComp(response));
    }
    if (nameScreen !== "" && groupFunction === "container") {
      config[groupFunction].screens[0].screens.forEach((screen) => {
        if (screen.name === nameScreen) {
          console.log(screen);
          response = [...screen.view.area];
        }
      });
      dispatch(setComp(response));
    }
  }, [groupFunction, config, nameScreen]);

  const handleChange = (e) => {
    const reader = new FileReader();
    reader.readAsText(e.target.files[0], "UTF-8");

    reader.onload = (e) => {
      try {
        const parsedConfig = JSON.parse(e.target.result);
        setConfig(parsedConfig);
        let response = [];
        response = [...parsedConfig[groupFunction].screen[0].view.area];
        console.log(response);

        // dispatch(
        //   setComp(parsedConfig.container.screens[0].screens[0].view.area)
        // );
        dispatch(setComp(response));
      } catch (error) {
        console.error("Lỗi khi phân tích cú pháp JSON:", error);
      }
    };
  };

  return (
    <>
      <div className="readConfigFile">
        <input type="file" id="file" required onChange={handleChange} hidden />

        <div className="text-black mb-3 textArea">
          {/* <pre>{JSON.stringify(config, null, 2)}</pre> */}
          <textarea
            cols="10"
            rows="500"
            className="text-area"
            value={JSON.stringify(config, null, 2)}
            readOnly={true}
            // onChange={handleTextAreaChange}
          />
        </div>

        <MobileDeviceFrame>
          <ScreenDisplay>
            <ElectricMeter2 />
          </ScreenDisplay>
        </MobileDeviceFrame>
      </div>
    </>
  );
}

export default ReadAppFile;
