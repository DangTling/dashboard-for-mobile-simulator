import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setComp } from "../redux/Components/compSlice";
import { toast } from "react-toastify";
import MobileDeviceFrame from "./MobileDeviceFrame";
import ScreenDisplay from "./ScreenDisplay";
import ElectricMeter2 from "./ElectricMeter2";
import { setItemWantFix } from "../redux/Components/itemWantFix";
import { setIndexWantFix } from "../redux/Components/indexWantFix";

function ReadConfigAPI() {
  const [config, setConfig] = useState(null);
  const [operationStatus, setOpeeerationStatus] = useState(false);
  const [activeBtn, setActiveBtn] = useState("edit");
  const [typeBtn, setTypeBtn] = useState("text");
  const [editAreaValue, setEditAreaValue] = useState();
  const [addAreaValue, setAddAreaValue] = useState("");
  const [inputValue, setInputValue] = useState("");
  const [apiUrl, setApiUrl] = useState("");
  const dispatch = useDispatch();
  const itemWantFix = useSelector((state) => state.itemWantFix);
  const indexWantFix = useSelector((state) => state.indexWantFix);

  useEffect(() => {
    if (config) {
      if (activeBtn === "edit") {
        setEditAreaValue(JSON.stringify(itemWantFix, null, 2));
        const newArr = config.detail.view.area.map((item, index) => {
          if (index === indexWantFix) {
            return itemWantFix;
          }
          return item;
        });

        config.detail.view.area = newArr;
        dispatch(setComp(config.detail.view.area));
      } else if (activeBtn === "add") {
        const newArr = [...config.detail.view.area];

        newArr.splice(indexWantFix, 0, itemWantFix);

        dispatch(setComp(newArr));
      }
    }
  }, [itemWantFix, activeBtn]);

  const handleInputChange = (e) => {
    const newValue = e.target.value;
    setInputValue(newValue);
  };

  const handleActiveBtn = (type) => {
    setActiveBtn(type);
  };

  const handleTypeBtn = (type) => {
    setTypeBtn(type);
    switch (type) {
      case "text":
        setAddAreaValue(`
        {
          "x": ,
          "y": ,
          "width": ,
          "height": ,
          "type": "text",
          "fontSize": ,
          "value": " ",
          "text_style": {
            "color": " ",
            "fontWeight": " "
          }
        }
        `);
        break;
      case "image":
        setAddAreaValue(`
        {
          "x": ,
          "y": ,
          "width": ,
          "height": ,
          "type": "image",
          "value": [" "],
          "map_data": {
            "1": {
              "uri": " "
            },
            "2": {
              "uri": " "
            },
            "3": {
              "uri": " "
            },
            "4": {
              "uri": " "
            },
            "default": {
              "uri": " "
            }
          },
          "image_style": {
            "width": "100%",
            "height": "100%"
          }
        }
        `);
        break;
      case "view":
        setAddAreaValue(`
        {
          "x": ,
          "y": ,
          "width": ,
          "height": ,
          "type": "view",
          "style": {
            "backgroundColor": " ",
            "borderRadius": 
          }
        }
        `);
        break;
      case "isConditionComp":
        setAddAreaValue(`
        {
          "isConditionComponent": true,
          "value": [" "],
          "map_item": [
            {
              "value": "true",
              "item": {
                "x": ,
                "y": ,
                "width": ,
                "height": ,
                "type": " ",
                "value": [" "],
                "text_style": {
                  "color": " ",
                  "fontWeight": 
                },
                "fontSize": ,
                "style": {
                  "alignItems": "flex-end"
                }
              }
            },
            {
              "value": "false",
              "item": {
                "x": ,
                "y": ,
                "width": ,
                "height": ,
                "type": " ",
                "value": [" "],
                "text_style": {
                  "color": " ",
                  "fontWeight": 
                },
                "fontSize": ,
                "style": {
                  "alignItems": "flex-end"
                }
              }
            },
            {
              "isDefault": true,
              "item": {
                "x": ,
                "y": ,
                "width": ,
                "height": ,
                "type": "",
                "value": [" "],
                "text_style": {
                  "color": " ",
                  "fontWeight": 
                },
                "fontSize": ,
                "style": {
                  "alignItems": "flex-end"
                }
              }
            }
          ]
        }
        `);
      default:
        break;
    }
  };

  const handleChange = (apiUrl) => {
    const requestOptions = {
      method: "GET",
      headers: {
        token:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIwMDk1ZmViNi0wZGM5LTRmN2EtOGUxNy0zZjg0ZDc2MGEwMzkiLCJlbWFpbCI6InRydW5ndHRAbWh0LnZuIiwiYXBwQ29kZSI6IkNPTlNPTEVfRklMSU9UIiwidmVuZG9yQ29kZSI6bnVsbCwiYXBwVHlwZSI6IkNPTlNPTEUiLCJpYXQiOjE2OTg3Mzg3MDIsImV4cCI6MTY5ODc2MDMwMn0.G5L8oUI65a4wtYDJtKlmMLL4A-LgDOTYOkz7TVxbe9Q",
      },
    };
    if (apiUrl) {
      fetch(apiUrl, requestOptions)
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network response was not ok");
          }
          return response.json();
        })
        .then((data) => {
          try {
            const parsedConfig = data.items[0].info;
            setConfig(parsedConfig);
            dispatch(setComp(parsedConfig.detail.view.area));

            // console.log(parsedConfig);
          } catch (error) {
            console.error("Lỗi khi xử lý dữ liệu JSON:", error);
          }
        })
        .catch((error) => {
          console.error("Lỗi khi gửi yêu cầu đến API:", error);
        });
    }

    // const reader = new FileReader();
    // reader.readAsText(e.target.files[0], "UTF-8");
    // reader.onload = (e) => {
    //   try {
    //     const parsedConfig = JSON.parse(e.target.result);
    //     setConfig(parsedConfig);
    //     dispatch(setComp(parsedConfig.detail.view.area));
    //   } catch (error) {
    //     console.error("Lỗi khi phân tích cú pháp JSON:", error);
    //   }
    // };
  };

  const handleTextAreaChange = (e) => {
    const newValue = e.target.value;
    setEditAreaValue(newValue);
    try {
      const newItem = JSON.parse(e.target.value);
      dispatch(setItemWantFix(newItem));
    } catch (error) {
      console.log("Wait for it!");
    }
  };

  const handleAddAreaChange = (e) => {
    const newValue = e.target.value;
    setAddAreaValue(newValue);
  };

  const handleStopOperation = () => {
    setOpeeerationStatus(!operationStatus);
  };

  const handleSubmit = () => {
    try {
      const newItem = JSON.parse(addAreaValue);
      dispatch(setItemWantFix(newItem));
    } catch (error) {
      console.log("Wait for it");
    }
    dispatch(setIndexWantFix(inputValue));
  };

  return (
    <>
      <button
        className="btn btn-primary btnCustom"
        onClick={() => handleStopOperation()}
      >
        Custom
      </button>
      <div className="input-link d-flex justify-content-between">
        <input
          type="text"
          required
          value={apiUrl}
          onChange={(e) => setApiUrl(e.target.value)}
          style={{ width: "80%" }}
        />
        <button
          className="btn btn-primary"
          onClick={() => handleChange(apiUrl)}
        >
          Xử lý API
        </button>
      </div>

      <div className="readConfigFile">
        {config && operationStatus === false ? (
          <div className="text-black mb-3 textArea">
            {/* <pre>{JSON.stringify(config, null, 2)}</pre> */}
            <textarea
              cols="10"
              rows="500"
              className="text-area"
              value={JSON.stringify(config, null, 2)}
              readOnly={true}
              // onChange={handleTextAreaChange}
            />
          </div>
        ) : (
          <>
            <div className="btnGroup">
              <button
                className={`btn ${activeBtn === "edit" ? "active" : ""}`}
                onClick={() => handleActiveBtn("edit")}
              >
                Edit Component
              </button>
              <button
                className={`btn ${activeBtn === "add" ? "active" : ""}`}
                onClick={() => handleActiveBtn("add")}
              >
                Add Component
              </button>
            </div>
            {activeBtn === "edit" ? (
              <>
                <textarea
                  className="editArea"
                  readOnly={false}
                  onChange={handleTextAreaChange}
                  value={editAreaValue}
                />
              </>
            ) : (
              <>
                <div className="optionComp">
                  <button
                    className={`btn ${typeBtn === "text" ? "active" : ""}`}
                    onClick={() => handleTypeBtn("text")}
                  >
                    Text
                  </button>
                  <button
                    className={`btn ${typeBtn === "image" ? "active" : ""}`}
                    onClick={() => handleTypeBtn("image")}
                  >
                    Image
                  </button>
                  <button
                    className={`btn ${typeBtn === "view" ? "active" : ""}`}
                    onClick={() => handleTypeBtn("view")}
                  >
                    View
                  </button>
                  <button
                    className={`btn ${
                      typeBtn === "isConditionComp" ? "active" : ""
                    }`}
                    onClick={() => handleTypeBtn("isConditionComp")}
                  >
                    Condition Comp
                  </button>
                </div>
                <textarea
                  className="editArea"
                  value={addAreaValue}
                  onChange={handleAddAreaChange}
                />
                <input
                  type="number"
                  className="keyAdd"
                  placeholder="Key Of Comp"
                  onChange={handleInputChange}
                  value={inputValue}
                />
                <button
                  className="btn btn-primary"
                  onClick={() => handleSubmit()}
                >
                  Submit
                </button>
              </>
            )}
          </>
        )}
        <MobileDeviceFrame>
          <ScreenDisplay>
            <ElectricMeter2 />
          </ScreenDisplay>
        </MobileDeviceFrame>
      </div>
    </>
  );
}

export default ReadConfigAPI;
