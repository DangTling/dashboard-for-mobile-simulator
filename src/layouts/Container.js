import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import logo from "../assets/image/download.png";
import { logout } from "../redux/Client/userSlice";
import { useNavigate } from "react-router-dom";
import { setStateDashBoard } from "../redux/Client/stateDashboardSlice";
import { setCompForSidebar } from "../redux/Client/compForSidebarSlice";
import { setCompForContainer } from "../redux/Client/compForContainerSlice";

function Container({ children }) {
  const [searchValue, setSearchValue] = useState("");
  const [deleteSearch, setDeleteSearch] = useState(false);
  const user = useSelector((state) => state.user);
  const stateDashBoard = useSelector((state) => state.stateDashBoard);
  const [userMenu, setUserMenu] = useState(false);
  const userAvatarRef = useRef(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [newNavItem, setNewNavItem] = useState({
    id: "",
    label: "",
    class: "nav-child",
    icon: "",
  });
  const [newContainerItem, setNewContainerItem] = useState({
    id: "",
    width: "",
    height: "",
    chartData: null,
  });

  const handleLogout = () => {
    dispatch(logout());
  };

  const handleToggleMenu = () => {
    setUserMenu(!userMenu);
  };

  const handleClickOutside = (event) => {
    if (
      userAvatarRef.current &&
      !userAvatarRef.current.contains(event.target)
    ) {
      setUserMenu(false);
    }
  };

  const handleSaveChange = () => {
    dispatch(setCompForSidebar(newNavItem));
    dispatch(setCompForContainer(newContainerItem));
    setNewNavItem({
      id: "",
      label: "",
      class: "nav-child",
      icon: "",
    });
    setNewContainerItem({
      id: "",
      width: "",
      height: "",
      chartData: null,
    });
  };

  useEffect(() => {
    if (searchValue === "") {
      setDeleteSearch(false);
    } else {
      setDeleteSearch(true);
    }
    document.addEventListener("click", handleClickOutside);

    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [searchValue]);

  return (
    <div className="containerAll">
      <div className="headerContainer">
        <div className="searchInput">
          <FontAwesomeIcon
            icon="fa-solid fa-magnifying-glass"
            className="iconSearch"
          />
          <input
            type="text"
            placeholder="Search ..."
            onChange={(e) => setSearchValue(e.target.value)}
            value={searchValue}
          />
          {deleteSearch ? (
            <FontAwesomeIcon
              icon="fa-solid fa-circle-xmark"
              className="iconDelete"
              onClick={() => setSearchValue("")}
            />
          ) : (
            <></>
          )}
        </div>
        {stateDashBoard === true ? (
          <>
            <button
              type="button"
              className="btn btn-success"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              Add comp for sidebar
            </button>
            <button
              type="button"
              className="btn btn-danger"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal2"
            >
              Add comp for container
            </button>
          </>
        ) : (
          <></>
        )}
        <div className="userInfo">
          <div className="iconUser">
            <FontAwesomeIcon icon="fa-solid fa-bell" className="iconBell" />
            <FontAwesomeIcon icon="fa-solid fa-comments" />
          </div>

          <div className="userAvatar" onClick={() => handleToggleMenu()}>
            <img src={user.auth ? user.avatar : logo} alt="" />
          </div>
        </div>
        {userMenu && (
          <div className="userMenu">
            <ul>
              <li className="li-title">Settings</li>
              <li className="li-child">
                <FontAwesomeIcon className="icon" icon="fa-solid fa-user" />
                <span>Profile</span>
              </li>
              <li
                className="li-child"
                onClick={() => {
                  dispatch(setStateDashBoard(!stateDashBoard));
                  handleToggleMenu();
                }}
              >
                <FontAwesomeIcon className="icon" icon="fa-solid fa-gear" />
                {stateDashBoard === false ? (
                  <span>Config Dashboard</span>
                ) : (
                  <span>Confirm this Change</span>
                )}
              </li>
              <li className="li-child">
                <FontAwesomeIcon
                  className="icon"
                  icon="fa-regular fa-credit-card"
                />
                <span>Payments</span>
              </li>
              <li className="li-child">
                <FontAwesomeIcon
                  className="icon"
                  icon="fa-regular fa-folder-open"
                />
                <span>Projects</span>
              </li>
              <li className="li-child li-logout" onClick={() => handleLogout()}>
                <FontAwesomeIcon
                  className="icon"
                  icon="fa-solid fa-right-from-bracket"
                />
                <span>Logout</span>
              </li>
            </ul>
          </div>
        )}
      </div>
      <div className="containContainer">{children}</div>
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalCenterTitle">
                Add New Comp
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label
                    for="recipient-id"
                    className="col-form-label"
                    style={{ float: "left" }}
                  >
                    ID:
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="recipient-id"
                    placeholder="Ex: charts, googleMap, ..."
                    onChange={(e) =>
                      setNewNavItem({ ...newNavItem, id: e.target.value })
                    }
                    value={newNavItem.id}
                  />
                </div>
                <div className="form-group">
                  <label
                    for="recipient-name"
                    className="col-form-label"
                    style={{ float: "left" }}
                  >
                    Label:
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="recipient-name"
                    placeholder="Ex: Charts, Google Map, ..."
                    onChange={(e) =>
                      setNewNavItem({ ...newNavItem, label: e.target.value })
                    }
                    value={newNavItem.label}
                  />
                </div>
                <div
                  className="form-group"
                  style={{
                    display:
                      newNavItem.class === "nav-child" ? "block" : "none",
                  }}
                >
                  <label
                    for="recipient-icon"
                    className="col-form-label"
                    style={{
                      float: "left",
                    }}
                  >
                    icon:
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="recipient-icon"
                    placeholder="Ex: fa-solid fa-map-location-dot, fa-solid fa-box-open, ..."
                    onChange={(e) =>
                      setNewNavItem({ ...newNavItem, icon: e.target.value })
                    }
                    value={newNavItem.icon}
                  />
                </div>
                <div className="form-group">
                  <label
                    for="class"
                    className="col-form-label"
                    style={{ float: "left" }}
                  >
                    Class name:
                  </label>
                  <select
                    id="class"
                    className="form-control"
                    onChange={(e) =>
                      setNewNavItem({ ...newNavItem, class: e.target.value })
                    }
                    value={newNavItem.class}
                  >
                    <option value="nav-child">nav-child</option>
                    <option value="nav-title">nav-title</option>
                  </select>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => handleSaveChange()}
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="exampleModal2"
        tabIndex="-1"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalCenterTitle">
                Add New Comp
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label
                    for="recipient-id"
                    className="col-form-label"
                    style={{ float: "left" }}
                  >
                    ID:
                  </label>
                  <input
                    type="number"
                    className="form-control"
                    id="recipient-id"
                    placeholder="Enter your position you want"
                    onChange={(e) =>
                      setNewContainerItem({
                        ...newContainerItem,
                        id: e.target.value,
                      })
                    }
                    value={newContainerItem.id}
                  />
                </div>
                <div className="form-group">
                  <label
                    for="recipient-name"
                    className="col-form-label"
                    style={{ float: "left" }}
                  >
                    Width:
                  </label>
                  <input
                    type="number"
                    className="form-control"
                    id="recipient-name"
                    placeholder="Enter width for this component"
                    onChange={(e) =>
                      setNewContainerItem({
                        ...newContainerItem,
                        width: e.target.value,
                      })
                    }
                    value={newContainerItem.width}
                  />
                </div>
                <div className="form-group">
                  <label
                    for="recipient-icon"
                    className="col-form-label"
                    style={{
                      float: "left",
                    }}
                  >
                    Height:
                  </label>
                  <input
                    type="number"
                    className="form-control"
                    id="recipient-icon"
                    placeholder="Enter height for this component"
                    onChange={(e) =>
                      setNewContainerItem({
                        ...newContainerItem,
                        height: e.target.value,
                      })
                    }
                    value={newContainerItem.height}
                  />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => handleSaveChange()}
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Container;
