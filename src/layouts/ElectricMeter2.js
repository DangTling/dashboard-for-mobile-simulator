import { useDispatch, useSelector } from "react-redux";
import Image from "./Image";
import Text from "./Text";
import View from "./View";

import IsConditionComponent from "./isConditionComponent";

import ScrollView from "./ScrollView";
import { useEffect, useState } from "react";
import { setItemWantFix } from "../redux/Components/itemWantFix";

import { setIndexWantFix } from "../redux/Components/indexWantFix";
import { setGroupFunction } from "../redux/Components/groupFunctionSlice";
import { setNameScreen } from "../redux/Components/nameScreenSlice";

function ElectricMeter2() {
  const comp = useSelector((state) => state.comp);
  const itemWantFix = useSelector((state) => state.itemWantFix);
  const dispatch = useDispatch();
  const [displayed, setDisplayed] = useState(false);
  // const [itemWantFix, setItemWantFix] = useState({});

  const handleClick = (item, index) => {
    if (item.onPress && item.onPress.execute) {
      switch (item.onPress.execute[0].name) {
        case "wellcome.deactive":
          dispatch(setGroupFunction("auth"));
          break;
        case "fAuth.signIn":
          dispatch(setGroupFunction("container"));
          break;
        case "navigator.navigate":
          dispatch(setNameScreen(item.onPress.execute[0].args.screen));
          break;
        default:
          break;
      }
    }
    dispatch(setItemWantFix(item));
    dispatch(setIndexWantFix(index));
  };
  const handleChange = (e) => {
    try {
      const newItem = JSON.parse(e.target.value);
      dispatch(setItemWantFix(newItem));
    } catch (error) {
      console.log("Wait for it!");
    }
  };
  const handleSubmit = () => {
    setDisplayed(false);
  };
  return (
    <>
      {comp &&
        comp.map((item, index) => {
          console.log(item);
          if (item.type === "text") {
            return (
              <Text
                // key={index}
                x={item.x}
                y={item.y}
                width={item.width}
                height={item.height}
                value={item.value}
                fontSize={`${item.fontSize}px`}
                color={
                  item.text_style && item.text_style.color
                    ? item.text_style.color
                    : "black"
                }
                fontWeight={
                  item.text_style && item.text_style.fontWeight
                    ? item.text_style.fontWeight
                    : "400"
                }
                zIndex={index}
                backgroundColor={
                  item.style && item.style.backgroundColor
                    ? item.style.backgroundColor
                    : ""
                }
                alignItems={
                  item.style && item.style.alignItems
                    ? item.style.alignItems
                    : ""
                }
                onClick={() => handleClick(item, index)}
              />
              // <>
              //   <h1>{item.value}</h1>
              //   <h1>{item.y}</h1>
              // </>
            );
          } else if (item.type === "image") {
            return (
              <Image
                // key={index}
                x={item.x}
                y={item.y}
                width={item.width}
                height={item.height}
                uri={
                  item.map_data.default && item.map_data.default.uri
                    ? item.map_data.default.uri
                    : item.map_data.true && item.map_data.true.uri
                    ? item.map_data.true.uri
                    : item.map_data[item.value] && item.map_data[item.value].uri
                    ? item.map_data[item.value].uri
                    : ""
                }
                mapData={item.map_data}
                zIndex={index}
                alignItems={
                  item.style && item.style.alignItems
                    ? item.style.alignItems
                    : ""
                }
                resizeMode={
                  item.image_style.resizeMode === "stretch" ? "" : "contain"
                }
                func={item.function ? item.function : {}}
                dataType={item.data_type ? item.data_type : ""}
                value={item.value ? item.value[0] : ""}
                onClick={() => handleClick(item, index)}
              />
              // <></>
            );
          } else if (item.isConditionComponent) {
            return (
              <IsConditionComponent
                zIndex={index}
                value={item.value}
                mapItem={item.map_item}
                ind={index}
              />
              // <></>
            );
          } else if (item.type === "scroll_view") {
            return <ScrollView areas={item.areas} />;
          } else if (item.type === "view") {
            return (
              <View
                x={item.x}
                y={item.y}
                width={item.width}
                height={item.height}
                backgroundColor={
                  item.style && item.style.backgroundColor
                    ? item.style.backgroundColor
                    : ""
                }
                borderRadius={`${
                  item.style && item.style.borderRadius
                    ? item.style.borderRadius
                    : ""
                }px`}
                zIndex={index}
                onClick={() => handleClick(item, index)}
              />
              // <></>
            );
          }
        })}
      {displayed && (
        <>
          <textarea
            className={`textBox ${displayed ? "scaled" : ""}`}
            onChange={handleChange}
          >
            {JSON.stringify(itemWantFix, null, 2)}
          </textarea>
          <button
            className={`btn btn-danger ${displayed ? "scaled" : ""}`}
            onClick={handleSubmit}
          >
            Finish
          </button>
        </>
      )}
    </>
  );
}

export default ElectricMeter2;
