import { useEffect, useState } from "react";
import logo from "../assets/image/coreui.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Dashboard from "./Dashboard";
import ReadConfigFiles from "./ReadConfigFiles";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { setCompForSidebar } from "../redux/Client/compForSidebarSlice";
import { setPageElements } from "../redux/Client/pageElementsSlice";
import ReadConfigAPI from "./ReadConfigAPI";
import ReadConfigFile from "./ReadAppFile";
import ReadAppFile from "./ReadAppFile";

function SideBar({ changeChild }) {
  const [isHidden, setIsHidden] = useState(false);
  const dispatch = useDispatch();
  const stateDashBoard = useSelector((state) => state.stateDashBoard);
  const newNavItem = useSelector((state) => state.compForSidebar);
  const pageElements = useSelector((state) => state.pageElements);

  const [navItem, setNavItem] = useState([
    {
      id: "dashboard",
      icon: "fa-solid fa-gauge",
      label: "Dashboard",
      class: "nav-child",
    },
    {
      id: "image",
      icon: "fa-regular fa-images",
      label: "Image",
      class: "nav-child",
    },
    {
      id: "text",
      icon: "fa-solid fa-font",
      label: "Text",
      class: "nav-child",
    },
    {
      id: "view",
      icon: "fa-solid fa-box-open",
      label: "View",
      class: "nav-child",
    },
    {
      id: "scrollView",
      icon: "fa-solid fa-hand-point-up",
      label: "Scroll View",
      class: "nav-child",
    },
    {
      id: "conditionComp",
      icon: "fa-solid fa-computer-mouse",
      label: "Condition Comp",
      class: "nav-child",
    },
    {
      id: "plugins",
      label: "PLUGINS",
      class: "nav-title",
    },
    {
      id: "calendar",
      icon: "fa-solid fa-calendar-days",
      label: "Calendar",
      class: "nav-child",
    },
    {
      id: "charts",
      icon: "fa-solid fa-chart-pie",
      label: "Charts",
      class: "nav-child",
    },
    {
      id: "dataTables",
      icon: "fa-solid fa-magnifying-glass-chart",
      label: "Data Tables",
      class: "nav-child",
    },
    {
      id: "googleMap",
      icon: "fa-solid fa-map-location-dot",
      label: "Google Map",
      class: "nav-child",
    },
    {
      id: "function",
      label: "Function",
      class: "nav-title",
    },
    {
      id: "import",
      icon: "fa-solid fa-file-import",
      label: "Import File",
      class: "nav-child",
    },
    {
      id: "export",
      icon: "fa-solid fa-download",
      label: "Export File",
      class: "nav-child",
    },
    {
      id: "readFromLink",
      icon: "fa-solid fa-link",
      label: "Read from API",
      class: "nav-child",
    },
    {
      id: "readAppFile",
      icon: "fa-solid fa-mobile-screen-button",
      label: "Read Config File",
      class: "nav-child",
    },
  ]);

  const handleRemoveNavItem = (item) => {
    let id = item.id;
    let updatedNavItems = navItem.filter((item) => item.id !== id);
    setNavItem(updatedNavItems);
    const updatedElements = { ...pageElements };
    if (updatedElements[item.id] !== undefined) {
      delete updatedElements[item.id];
    } else {
      console.log("Don't find this item");
    }
    dispatch(setPageElements(updatedElements));
  };

  const handleAppear = () => {
    setIsHidden(!isHidden);
  };
  const handleNavItemClick = (event, param) => {
    switch (param.id) {
      case "import":
        changeChild(<ReadConfigFiles />);
        break;

      case "readFromLink":
        changeChild(<ReadConfigAPI />);
        break;

      case "readAppFile":
        changeChild(<ReadAppFile />);
        break;

      default:
        changeChild(<Dashboard param={param} />);
        break;
    }
    const navChildElements = document.querySelectorAll(".nav-child");
    navChildElements.forEach((element) => {
      element.classList.remove("active");
    });

    event.currentTarget.classList.add("active");
  };

  const isUniqueId = (newItemId, existingItems) => {
    return existingItems.every((item) => item.id !== newItemId);
  };

  useEffect(() => {
    if (newNavItem.label === "" || newNavItem.id === "") {
      console.log("This comp has some error");
    } else if (isUniqueId(newNavItem.id, navItem)) {
      const updatedNavItems = [...navItem];
      updatedNavItems.push(newNavItem);
      setNavItem(updatedNavItems);
    } else {
      toast.error("ID must be uniqued");
    }
  }, [newNavItem]);

  return (
    <div className={`sideBar ${isHidden ? "hide" : ""}`}>
      <div className="header">
        <img src={logo} alt="coreui-logo" />
        {isHidden ? (
          <FontAwesomeIcon
            icon="fa-solid fa-chevron-right"
            className="backIcon"
            onClick={() => handleAppear()}
          />
        ) : (
          <FontAwesomeIcon
            icon="fa-solid fa-chevron-left"
            className="backIcon"
            onClick={() => handleAppear()}
          />
        )}
      </div>
      <div className="sideBarContent">
        {navItem.map((item) => {
          if (item.class === "nav-title") {
            return (
              <li className={item.class} key={item.id}>
                {item.label}
                {stateDashBoard === true ? (
                  <button
                    className="remove-nav-item"
                    onClick={() => handleRemoveNavItem(item)}
                  >
                    <FontAwesomeIcon icon="fa-solid fa-trash" />
                  </button>
                ) : (
                  <></>
                )}
              </li>
            );
          } else {
            return (
              <li
                className={item.class}
                key={item.id}
                onClick={(e) => handleNavItemClick(e, item)}
              >
                <FontAwesomeIcon
                  icon={item.icon}
                  className={`icon ${isHidden ? "show" : ""}`}
                />
                {item.id === "import" || item.id === "readAppFile" ? (
                  <label htmlFor="file">{item.label}</label>
                ) : (
                  <span>{item.label}</span>
                )}

                {stateDashBoard === true ? (
                  <button
                    className="remove-nav-item"
                    onClick={() => handleRemoveNavItem(item)}
                  >
                    <FontAwesomeIcon icon="fa-solid fa-trash" />
                  </button>
                ) : (
                  <></>
                )}
              </li>
            );
          }
        })}
      </div>
    </div>
  );
}

export default SideBar;
