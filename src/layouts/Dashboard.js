import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { setPageElements } from "../redux/Client/pageElementsSlice";

function Dashboard(props) {
  const { param } = props;
  const compForContainer = useSelector((state) => state.compForContainer);
  const stateDashBoard = useSelector((state) => state.stateDashBoard);
  const compForSidebar = useSelector((state) => state.compForSidebar);
  const pageElements = useSelector((state) => state.pageElements);
  const [cardWeatherData, setCardWeatherData] = useState({});
  const dispatch = useDispatch();

  const handleRemoveContainerItem = (id) => {
    const updatedElements = [...pageElements[param.id]];
    updatedElements.splice(id, 1);
    dispatch(
      setPageElements({
        ...pageElements,
        [param.id]: updatedElements,
      })
    );
  };

  const handleAddContent = async (index) => {
    try {
      const updatedElements = [...pageElements[param.id]];
      let elementToEdit = { ...updatedElements[index] };
      const request1 = axios.get(
        "https://api.weatherapi.com/v1/current.json?key=9461bf4a5de64f2483294638231610&q=hanoi&days=5&aqi=no&alerts=no"
      );
      // Yêu cầu dữ liệu từ API thứ hai
      const request2 = axios.get(
        "https://api.openweathermap.org/data/2.5/weather?q=hanoi&units=metric&appid=8b5e5543cd14da6ac4e9d9bb2285e585"
      );
      // const response = await axios.get(
      //   "http://api.weatherapi.com/v1/current.json?key=9461bf4a5de64f2483294638231610&q=hanoi&aqi=no"
      // );
      // const weatherData = response.data;

      // const tableData = {
      //   city: weatherData.location.name,
      //   country: weatherData.location.country,
      //   temperatureC: weatherData.current.temp_c,
      //   temperatureF: weatherData.current.temp_f,
      //   condition: weatherData.current.condition.text,
      //   humidity: weatherData.current.humidity,
      //   wind: `${weatherData.current.wind_kph} km/h ${weatherData.current.wind_dir}`,
      // };

      const [response1, response2] = await Promise.all([request1, request2]);
      const data1 = response1.data;
      const data2 = response2.data;

      let tableData = {
        city1: {
          name: data1.location.name,
          country: data1.location.country,
          temperatureC: data1.current.temp_c,
          temperatureF: data1.current.temp_f,
          condition: data1.current.condition.text,
          humidity: data1.current.humidity,
          wind: `${data1.current.wind_kph} km/h ${data1.current.wind_dir}`,
        },
        city2: {
          name: data2.name,
          country: data2.sys.country,
          temperatureC: data2.main.temp,
          temperatureF: (data2.main.temp * 9) / 5 + 32,
          condition: data2.weather[0].description,
          humidity: data2.main.humidity,
          wind: `${data2.wind.speed} m/s ${data2.wind.deg}°`,
        },
      };
      elementToEdit.chartData = tableData;
      console.log(tableData);
      updatedElements[index] = elementToEdit;
      dispatch(
        setPageElements({
          ...pageElements,
          [param.id]: updatedElements,
        })
      );
    } catch (error) {
      console.error("Lỗi khi lấy dữ liệu thời tiết: ", error);
    }
  };

  useEffect(() => {
    if (compForSidebar) {
      dispatch(
        setPageElements({
          ...pageElements,
          [compForSidebar.id]: [],
        })
      );
    }

    if (
      compForContainer.width === "" ||
      compForContainer.height === "" ||
      compForContainer.id === ""
    ) {
      console.log("This comp has some error");
    } else {
      const updatedElements = [...pageElements[param.id]];
      const indexToInsert = compForContainer.id - 1;

      if (indexToInsert >= 0) {
        updatedElements.splice(indexToInsert, 0, compForContainer);
      } else {
        toast.error("ID doesn't fit for this case");
      }

      dispatch(
        setPageElements({
          ...pageElements,
          [param.id]: updatedElements,
          // [compForSideba.id]: [],
        })
      );
    }
  }, [compForContainer, compForSidebar]);

  return (
    <div className="dashboard">
      {pageElements[param.id] ? (
        <>
          <h2 className="title text-uppercase"> {param.label} </h2>
          <p>
            <span className="textWeight">Home </span>/
            <span> {param.label} </span>
          </p>
          <div className="containDashboard">
            {pageElements[param.id].map((item, index) => {
              return (
                <div
                  className="card"
                  style={{ width: `${item.width}%`, height: `${item.height}%` }}
                >
                  {stateDashBoard === true ? (
                    <>
                      <button
                        className="remove-item"
                        onClick={() => handleRemoveContainerItem(index)}
                      >
                        <FontAwesomeIcon icon="fa-solid fa-trash" />
                      </button>
                      <button
                        className="add-content"
                        onClick={() => handleAddContent(index)}
                      >
                        <FontAwesomeIcon icon="fa-solid fa-circle-plus" />
                      </button>
                    </>
                  ) : (
                    <></>
                  )}

                  {item.chartData && (
                    <table className="table table-striped">
                      <thead className="thead-dark">
                        <tr>
                          <th>City</th>
                          <th>Country</th>
                          <th>Temperature (°C)</th>
                          <th>Temperature (°F)</th>
                          <th>Condition</th>
                          <th>Humidity</th>
                          <th>Wind</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{item.chartData.city1.name}</td>
                          <td>{item.chartData.city1.country}</td>
                          <td>{item.chartData.city1.temperatureC}</td>
                          <td>{item.chartData.city1.temperatureF}</td>
                          <td>{item.chartData.city1.condition}</td>
                          <td>{item.chartData.city1.humidity}</td>
                          <td>{item.chartData.city1.wind}</td>
                        </tr>
                        <tr>
                          <td>{item.chartData.city2.name}</td>
                          <td>{item.chartData.city2.country}</td>
                          <td>{item.chartData.city2.temperatureC}</td>
                          <td>{item.chartData.city2.temperatureF}</td>
                          <td>{item.chartData.city2.condition}</td>
                          <td>{item.chartData.city2.humidity}</td>
                          <td>{item.chartData.city2.wind}</td>
                        </tr>
                      </tbody>
                    </table>
                  )}
                </div>
              );
            })}
          </div>
        </>
      ) : (
        <div class="flex-container">
          <div class="text-center">
            <h1>
              <span class="fade-in" id="digit1">
                4
              </span>
              <span class="fade-in" id="digit2">
                0
              </span>
              <span class="fade-in" id="digit3">
                4
              </span>
            </h1>
            <h3 class="fadeIn">PAGE NOT FOUND</h3>
            <button type="button" name="button">
              Return To Home
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default Dashboard;
