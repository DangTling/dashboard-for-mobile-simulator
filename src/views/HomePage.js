import SideBar from "../layouts/SideBar";
import "../assets/styles/HomePage.scss";
import Container from "../layouts/Container";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { login } from "../redux/Client/userSlice";
import Dashboard from "../layouts/Dashboard";

function HomePage() {
  const [selectedChild, setSelectedChild] = useState(
    <Dashboard param={{ id: "dashboard", label: "Dashboard" }} />
  );
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const changeChild = (child) => {
    setSelectedChild(child);
  };
  useEffect(() => {
    if (localStorage.getItem("username") === null) {
      navigate("/login");
    } else {
      dispatch(
        login({ username: localStorage.getItem("username"), pass: "123456a@" })
      );
    }
  }, [user]);
  return (
    <>
      <SideBar changeChild={changeChild} />
      <Container>{selectedChild}</Container>
    </>
  );
}

export default HomePage;
