import { Routes, Route } from "react-router-dom";
import Login from "../views/Login";
import Register from "../views/Register";
import NotFound from "../views/NotFound";
import HomePage from "../views/HomePage";

function AppRoutes() {
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/" element={<HomePage />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default AppRoutes;
