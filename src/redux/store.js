import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./Client/userSlice";
import compReducer from "./Components/compSlice";
import valueOfTempReducer from "./Components/valueOfTempSlice";
import valueOfOn_OffReducer from "./Components/valueOfOn_OffSlice";

import valueOfParamMode from "./Components/valueOfParamMode";
import valueOfParamSwing from "./Components/valueOfParamSwing";
import valueOfParamFan_speed from "./Components/valueOfParamFan_speed";

import itemWantFix from "./Components/itemWantFix";
import indexWantFix from "./Components/indexWantFix";
import stateDashboardSlice from "./Client/stateDashboardSlice";
import compForSidebarSlice from "./Client/compForSidebarSlice";
import compForContainerSlice from "./Client/compForContainerSlice";
import pageElementsSlice from "./Client/pageElementsSlice";
import nameScreenSlice from "./Components/nameScreenSlice";
import groupFunctionSlice from "./Components/groupFunctionSlice";

const store = configureStore({
  reducer: {
    user: userSlice,
    comp: compReducer,
    valueOfTemp: valueOfTempReducer,
    valueOfOn_Off: valueOfOn_OffReducer,
    valueOfParamMode: valueOfParamMode,
    valueOfParamSwing: valueOfParamSwing,
    valueOfParamFan_speed: valueOfParamFan_speed,
    itemWantFix: itemWantFix,
    indexWantFix: indexWantFix,
    stateDashBoard: stateDashboardSlice,
    compForSidebar: compForSidebarSlice,
    compForContainer: compForContainerSlice,
    pageElements: pageElementsSlice,
    nameScreen: nameScreenSlice,
    groupFunction: groupFunctionSlice,
  },
});

export default store;
