import { createSlice } from "@reduxjs/toolkit";

const pageElementsSlice = createSlice({
  name: "pageElements",
  initialState: {
    dashboard: [
      {
        width: 100,
        height: 40,
        chartData: null,
      },
      {
        width: 47,
        height: 30,
        chartData: null,
      },
      {
        width: 48,
        height: 30,
        chartData: null,
      },
      {
        width: 20,
        height: 25,
        chartData: null,
      },
      {
        width: 30,
        height: 25,
        chartData: null,
      },
      {
        width: 40,
        height: 25,
        chartData: null,
      },
    ],
    image: [
      {
        width: 30,
        height: 25,
        chartData: null,
      },
      {
        width: 40,
        height: 25,
        chartData: null,
      },
    ],
    text: [],
    view: [],
    scrollView: [],
    conditionComp: [],
  },
  reducers: {
    setPageElements: (state, action) => {
      return action.payload;
    },
  },
});

export const { setPageElements } = pageElementsSlice.actions;
export default pageElementsSlice.reducer;
