import { createSlice } from "@reduxjs/toolkit";

const stateDashboardSlice = createSlice({
  name: "stateDashboard",
  initialState: false,
  reducers: {
    setStateDashBoard: (state, action) => {
      return action.payload;
    },
  },
});

export const { setStateDashBoard } = stateDashboardSlice.actions;
export default stateDashboardSlice.reducer;
