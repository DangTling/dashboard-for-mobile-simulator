import { createSlice } from "@reduxjs/toolkit";

const compForContainerSlice = createSlice({
  name: "compForContainer",
  initialState: {
    id: "",
    width: "",
    height: "",
    chartData: null,
  },
  reducers: {
    setCompForContainer: (state, action) => {
      return action.payload;
    },
  },
});

export const { setCompForContainer } = compForContainerSlice.actions;
export default compForContainerSlice.reducer;
