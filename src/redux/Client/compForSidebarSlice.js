import { createSlice } from "@reduxjs/toolkit";

const compForSidebarSlice = createSlice({
  name: "compForSidebar",
  initialState: {
    id: "",
    label: "",
    class: "",
    icon: "",
  },
  reducers: {
    setCompForSidebar: (state, action) => {
      return action.payload;
    },
  },
});

export const { setCompForSidebar } = compForSidebarSlice.actions;
export default compForSidebarSlice.reducer;
