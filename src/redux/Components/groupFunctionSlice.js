import { createSlice } from "@reduxjs/toolkit";

const groupFunctionSlice = createSlice({
  name: "groupFunction",
  initialState: "welcome_screen",
  reducers: {
    setGroupFunction: (state, action) => {
      return action.payload;
    },
  },
});

export const { setGroupFunction } = groupFunctionSlice.actions;
export default groupFunctionSlice.reducer;
