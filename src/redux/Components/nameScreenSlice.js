import { createSlice } from "@reduxjs/toolkit";

const nameScreenSlice = createSlice({
  name: "nameScreen",
  initialState: "",
  reducers: {
    setNameScreen: (state, action) => {
      return action.payload;
    },
  },
});

export const { setNameScreen } = nameScreenSlice.actions;
export default nameScreenSlice.reducer;
